from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from .views import response
from .csui_helper import get_access_token, verify_user
from fitur_profile.models import Pengguna
# authentication
def auth_login(request):
    print("#==> auth_login ")

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        #call csui_helper
        access_token = get_access_token(username, password)
        print(access_token)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']
            # set session
            request.session['user_login'] = username
            request.session['access_token'] = access_token
            request.session['kode_identitas'] = kode_identitas
            request.session['role'] = role
            request.session['nama'] = "kosong"
            print("bismillah")
            request.session['email'] = "kosong"
            
            pengguna = Pengguna(kode_identitas=kode_identitas,nama="kosong",email="kosong")
            pengguna.save()
            
            messages.success(request, "Anda berhasil login")
            response['logged_in'] = True
            request.session['login'] = True
        else:
            messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('login:index'))

def auth_logout(request):
    request.session.flush()  # menghapus semua session
    messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('login:index'))