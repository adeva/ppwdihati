from django.conf.urls import url
from .views import masuk_login,index
from .custom_auth import auth_login, auth_logout
#url for app
urlpatterns = [
    url(r'^$', masuk_login, name='masuk_login'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^index', index, name='index'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]