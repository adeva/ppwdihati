from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
response = {}

def index(request):
    if 'user_login' in request.session.keys():
        set_data_for_session(request)
        username = request.session['user_login']
        return HttpResponseRedirect(reverse('profile:index'))
    else:
        html = 'fitur_login/SSO UI.html'
        return render(request, html, response)
def masuk_login(request):
    html = 'fitur_login/awal.html'
    return render(request, html, response)

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

   # if 'user_login' in request.session:
        #response['login'] = True
      #  return HttpResponseRedirect(reverse('login:das'))
    #else:
        #response['login'] = False
        #response['author'] = get_data_user(request, 'user_login')
#    html = 'fitur_login/login.html'
 #   return render(request, html, response)

