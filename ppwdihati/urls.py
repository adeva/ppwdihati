"""ppwdihati URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur_profile.urls as profile
import fitur_login.urls as login
import fitur_status.urls as status
import fitur_riwayat.urls as riwayat
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(profile,namespace='profile')),
    url(r'^$', RedirectView.as_view(url='login',permanent=True),name = 'root'),
    url(r'^login/', include(login, namespace='login')),
    url(r'^status/', include(status, namespace='status')),
    url(r'^riwayat/', include(riwayat, namespace='riwayat')),
    url(r'^search/', include('cariMahasiswa.urls', namespace='search')),
]
