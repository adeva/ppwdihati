from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import  MessageKu
from fitur_profile.models import Pengguna, KeahlianKu
from fitur_login.views import index
from django.contrib import messages
from fitur_profile.views import index
from django.urls import reverse
response = {}
def index(request,username):
    if 'user_login' in request.session.keys():
        nama_user = request.session['user_login']
        html = 'fitur_status/update_status.html'
        set_data_for_session(request)
        print(username)
        print(nama_user)
        if username == nama_user:
            try:
                pengguna = Pengguna.objects.get(nama = username)
            except Exception as e:
                pengguna = create_new_user(request)
            response['login'] = True
            response['status_form'] = Message_Form
            response['pengguna'] = True
        else:
            try:
                pengguna = Pengguna.objects.get(nama = username)
                statusnya = MessageKu.objects.filter(pengguna = pengguna).order_by('-id')
                response['status'] = statusnya
                response['message'] = statusnya
                response['pengguna'] = False
                print('username')
            except Exception as e:
                print('nama_user')
                return HttpResponseRedirect('/status/'+ nama_user)
        statusnya = MessageKu.objects.filter(pengguna = pengguna).order_by('-id')
        response['status'] = statusnya
        response['message'] = statusnya    
        return render(request, html, response)
    else:
        response['login'] = False
        return HttpResponseRedirect(reverse('login:index'))
      
def message_post(request):
    form = Message_Form(request.POST or None)
    user_name = request.session['user_login']
    if((request.method == 'POST') and form.is_valid()):
        response['message'] = request.POST['message']
        kode_identitas = request.session['kode_identitas']
        pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        status = MessageKu(pengguna = pengguna, message=response['message'])
        status.save()
        message = MessageKu.objects.filter(pengguna = pengguna).order_by('-id')
        response['message'] = message
    return HttpResponseRedirect('/status/'+ user_name)



def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna